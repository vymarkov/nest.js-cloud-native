FROM node:dubnium-alpine as build

ENV           WORKDIR=/usr/src/app
ENV           NODE_ENV=development
ENV           BLUEBIRD_DEBUG=0
WORKDIR       $WORKDIR

COPY .npmrc package.json package-lock.json ./

RUN npm ci

COPY tsconfig.json tslint.json tsconfig.build.json $WORKDIR/
COPY src $WORKDIR/src/

RUN npm run build

FROM node:dubnium-alpine as production

ENV                                     APP_ENV=production
ENV                                     PORT=3000
ENV                                     WORKDIR /usr/src/app

# unfortunately, we cannot use the environment variable in health check command
# HEALTHCHECK --interval=10s --timeout=5s --start-period=3s --retries=3 CMD curl --fail http://localhost:2021/liveness || exit 1

WORKDIR                                 $WORKDIR

COPY .npmrc package.json package-lock.json .env.example ./
COPY --from=build $WORKDIR/dist         $WORKDIR/dist

RUN npm ci --production

RUN apk add --no-cache curl

EXPOSE                                  $PORT

CMD ["npm", "start"]

/* eslint-disable no-mixed-spaces-and-tabs */
import { Controller, Get, OnApplicationShutdown } from '@nestjs/common';
import { AppService } from './app.service';

const logger = console;

@Controller()
export class AppController implements OnApplicationShutdown {
  constructor (private readonly appService: AppService) {}

  @Get()
  getHello (): string {
    return this.appService.getHello();
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  public onApplicationShutdown () {
  	logger.debug('I`ve received shutdown signal, add specific logic to close all connections ');
  }
}

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DebugService } from './debug/debug.service';
import { DebugModule } from './debug/debug.module';

@Module({
  imports: [DebugModule],
  controllers: [AppController],
  providers: [AppService, DebugService],
})
export class AppModule {}

// https://emojipedia.org/rocket/
import 'dotenv-safe/config'

import { ShutdownSignal } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import * as pino from 'pino';
import { AppModule } from './app.module';
import { config } from './config';

async function bootstrap (port = 3000, level = 'debug') {
  const logger = pino({ level });
  const app = await NestFactory.create(AppModule);
  app.useLogger(logger);
  app.enableShutdownHooks([ShutdownSignal.SIGINT, ShutdownSignal.SIGTERM]);

  await app.listen(port);
  logger.info({ port }, 'Server has been successfully started 🚀');

  setInterval(() => {
    logger.debug(
      `The server uses approximately ${Math.round(
        process.memoryUsage().rss / (1024 * 1024),
      )} MB`,
    );
  }, 5000);
}


bootstrap(config.httpPort, config.loglevel);
